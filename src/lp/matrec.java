/* $Header: /home/cs/guest/javagrp/netdesign/lp/matrec.java,v 1.2 1996/06/06 19:47:20 hma Exp $ */
/* $Log: matrec.java,v $
# Revision 1.2  1996/06/06  19:47:20  hma
# added package statement
#
# Revision 1.1  1996/05/21  02:04:15  hma
# Initial revision
# */

package lp;

public class matrec
{
  int row_nr;
  double value;
  public matrec(int r, double v) {
    row_nr = r;
    value = v;
  }
}
