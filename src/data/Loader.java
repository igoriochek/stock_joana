/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import lt.ktu.mockus.srgm.StartupApplet;


public class Loader {        
        //protected StartupApplet = apl;
        private StartupApplet apl;
        
        
        public Loader( StartupApplet apl ) {
            this.apl = apl;
        }
        
        public void getRealData2() {
            apl.stockRealData = new double [apl.MAX_STOCK_COUNT][];
            for( int x = 0; x < apl.MAX_STOCK_COUNT; x++ ) {
                if ( !apl.realData[x] ) continue;
                //String sql = "SELECT day,  price AS priceR FROM " + Config.pricetable + " WHERE num = " + apl.CurrentBandymasNum + " AND stock = " + x + " AND strategy = \"AR1\"  ORDER BY day";
                //String sql = "SELECT day,  price AS priceR FROM " + Config.pricetable + " WHERE stock = " + x + " ORDER BY day";
                String sql = "SELECT day,  price AS priceR FROM " + Config.pricetable + " WHERE stock = " + x + " AND strategy = \"AR1\"  ORDER BY day";
                System.out.println( sql );
                try {
                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                    Connection con = (Connection) DriverManager.getConnection(Config.url, Config.userName, Config.password);
                    Statement stmt = (Statement) con.createStatement();
                    
                    ResultSet rs = stmt.executeQuery(sql);
                    
                    apl.stockRealData[x] = new double[apl.termin];
                    
                    int i = 0;
                    while (rs.next()) {
                        
                        System.out.println( rs.getInt("day") + "   " + rs.getDouble("priceR") );
                        apl.stockRealData[x][i] = rs.getDouble("priceR");
                        if (apl.stockRealData[x][i] < 0.1 ) System.out.println( "0 price iz DB" );
                        
                        
                        i++;
                    }
                    //stmt.execute(sql);
                    stmt.close();
                    con.close();
                    //con.abort(null);
                } catch (Exception e) {
                        System.out.println("Error in loader class");
                        System.out.println(e.getMessage());
                }               
            }
            
        }

}
