/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import lt.ktu.mockus.srgm.StartupApplet;

/**
 *
 * @author igor
 */
public class Writer {
        
        
        //protected StartupApplet = apl;
        private StartupApplet apl;
        
        public Writer( StartupApplet apl ) {
            this.apl = apl;
        }
        
        public void writeSQL( String sql ) {
            try {
                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                    Connection con = (Connection) DriverManager.getConnection(Config.url, Config.userName, Config.password);
                    Statement stmt = (Statement) con.createStatement();
                    stmt.execute(sql);
                    stmt.close();
                    con.close();
                    //con.abort(null);
            } catch (Exception e) {
                    System.out.println( " SQL error " );
                    System.out.println(e.getMessage());
            }
        }
        
        public void createDatabase() {            
            writeSQL("DROP TABLE " + Config.tablePredict);
            writeSQL("DROP TABLE " + Config.tablePortfel);
            writeSQL("DROP TABLE " + Config.tableProfit);
            
            String sql = "CREATE TABLE IF NOT EXISTS `" + Config.tablePredict + "` (\n" +
                        "  `num` int(11) NOT NULL,\n" +
                        "  `day` int(11) NOT NULL,\n" +
                        "  `stock` int(11) NOT NULL,\n" +
                        "  `strategy` varchar(9) COLLATE utf8_lithuanian_ci NOT NULL,\n" +
                        "  `price` double NOT NULL,\n" +
                        "  `predict` double NOT NULL\n" +
                        ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;";
            writeSQL(sql);
             sql = "CREATE TABLE IF NOT EXISTS `" + Config.tablePortfel + "` (\n" +
                    "  `num` int(11) NOT NULL,\n" +
                    "  `day` int(11) NOT NULL,\n" +
                    "  `strategy` varchar(9) COLLATE utf8_lithuanian_ci NOT NULL,\n" +
                    "  `stock` int(11) NOT NULL,\n" +
                    "  `stockCount` int(11) NOT NULL\n" +
                    ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;";
            writeSQL(sql);
            sql = "CREATE TABLE IF NOT EXISTS `" + Config.tableProfit+"` (\n" +
                  "  `num` int(11) NOT NULL,\n"  +
                  "  `day` int(11) NOT NULL,\n"  +
                  "  `strategy_predict` varchar(9) COLLATE utf8_lithuanian_ci NOT NULL,\n" +
                  "  `strategy_bs` varchar(30) COLLATE utf8_lithuanian_ci NOT NULL,\n" +
                  "  `profit` double NOT NULL\n" +
                  ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;";
            writeSQL(sql);
        }
        
        
        
        public void writeExperimentPortfel( int num, int day, String strategy[], /*int stock,*/ int stockCount[][]  ) {
           String sql = "";
           for ( int j = 0; j < strategy.length; j++ ) {
            for ( int i = 0; i < stockCount[0].length; i++ ) {
                if ( sql.length() > 0 ) sql = sql + ",";
                sql = sql + "( " + num + "," + day + ", '" + strategy[j] + "', " + i + "," + stockCount[j][i] + " ) ";
            }
           }
            sql = "INSERT INTO `" + Config.tablePortfel + "` VALUES " + sql; 
            writeSQL(sql);
        }
        
        public void writeExperimentPaklaida( int num, int day, String strategy[], double price[], double predict[][] ) {  
            String sql = "";
           for ( int j = 0; j < strategy.length; j++ ) {
            for ( int i = 0; i < price.length; i++ ) {
                if ( sql.length() > 0 ) sql = sql + ",";
                if ( price[i] < 0.1 ) System.out.println("Writing price 0 to db "); 
                sql = sql + " ( " + num + "," + day + "," + i + ",'" + strategy[j] + "'," + price[i] + "," + predict[j][i] + " ) ";
            }
           }
            sql = "INSERT INTO `" + Config.tablePredict + "` VALUES " + sql;
            writeSQL(sql);
        }
        
        public void writePelnai( int num, int day, String [] strategy, String[] strategy2, double [] value ) {
           String sql = "";
           for ( int i = 0; i < strategy.length; i++ ) {
               if ( sql.length() > 0 ) sql = sql + ",";
               sql = sql + " ( " + num + "," + day + ", '" + strategy[i] + "', '" + strategy2[i] + "', '" + value[i] + "' ) ";
           }
           sql =  "INSERT INTO `" + Config.tableProfit + "` VALUES " + sql;
           writeSQL(sql);
        }
    
}
