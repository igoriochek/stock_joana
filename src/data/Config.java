package data;
/**
 *
 * @author igor
 */
public class Config {
    public static String url = "jdbc:mysql://localhost:3306/experiment";
    public static String userName = "root";
    public static String password = "";
    
    public static String expnum = "3";
    public static String tblnum = "17";
    
    //public static String readTableNum = "2";
    public static String postfix = "";
    
    public static String tablePortfel = "rep"+expnum+"_experiment_portfel_r"+tblnum+postfix;
    public static String tablePredict = "rep"+expnum+"_experiment_predict_r"+tblnum+postfix;
    public static String tableProfit =  "rep"+expnum+"_experiment_profit_r"+tblnum+postfix;
    
    //public static String pricetable = expnum+"experiment__strategy_predict_r"+tblnum;
    public static String pricetable = "rep2_experiment_predict_r1";
    
}
