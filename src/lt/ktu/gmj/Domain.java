// Domain.java

package lt.ktu.gmj;

public class Domain implements Customizable {
	public double min[];

	public double max[];

	public Point defaultPoint;

	int dimensions;
        
	public Domain() {
	}

	public int dimensions() {
            return dimensions;
	}

	public void CreateArray(int Dim) {
            dimensions = Dim;
            min = new double[dimensions];
            max = new double[dimensions];
            defaultPoint = new Point(this);
            for (int i = 0; i < dimensions; i++) {
                    min[i] = 0;
                    max[i] = 1;
                    defaultPoint.x[i] = 0;
            }
	}

	public final void normalizePoint(Point pt) {
		for (int i = 0; i < pt.x.length; i++)
			pt.x[i] = pt.x[i] * (max[i] - min[i]) + min[i];
	}

};