// Task.java
package lt.ktu.gmj;

import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;

import defaultPackage.MeGraphics;
import lt.ktu.mockus.srgm.StockContainer;

public interface Task extends Customizable {

	Domain domain();

	double f(Point pt);
        
        void setParameters( int Iterations[], int InType, double Resours,
			StockContainer stocksPrices[],  double  result[],  String mMethod, double bankrupt[], double profitability[] );

	/*void setParameters(int Iterations[], int InType, double Resours,
			double AllBankrupt[], double AllDividends[],
			JProgressBar wProgressBar, JLabel wALabel[],
			JTabbedPane wTabbedPane, JButton w1Button, JButton w2Button,
			int BankLength, int ShareLength, int InsuranceLength,
			double ObjectValue[], String mMethod, double Uf[]);*/

	void Data(Vector wItVector, Vector wFVector);

	void ResumeCounter();

	void SuspendCounter();

	void FinishedCounter();

	void CounterPoint(Thread Counter);

	//void FrameD();
};