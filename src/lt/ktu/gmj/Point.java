// Point.java
package lt.ktu.gmj;

public class Point {
	public double x[];

	public Point(Domain domain) {
            x = new double[domain.dimensions()]; // .length
	}

	public Point(Point pt) {
            x = new double[pt.x.length];
            System.arraycopy(pt.x, 0, x, 0, x.length);
	}

	public double distance(Point pt) {
            double dist = 0;
            for (int i = 0; i < x.length; i++) {
		double s = x[i] - pt.x[i];
		dist += s * s;
            }
            return Math.sqrt(dist);
	}

	public double norma() {
		double s = 0;
		for (int i = 0; i < x.length; i++)
			s += x[i] * x[i];
		return Math.sqrt(s);
	}

};