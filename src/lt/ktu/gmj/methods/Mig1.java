// Mig1.java

package lt.ktu.gmj.methods;

import lt.ktu.gmj.*;

public class Mig1 extends MethodBase {

 public Mig1 ()
 {
   iterations=1000;
 }

public void setParameters(int _iterations, int _initialPoints){
	iterations = _iterations; 
	initialPoints = _initialPoints;
}

 public Result run (ResultLogger logger, Task task)
 {
   Domain domain = task.domain();
   Result result = new Result();

    for (int iteration = 0; iteration < iterations; iteration++)
    {
      Point pt = new RandomPoint(domain);
      Result r = new Result (iteration, pt, task.f(pt));
      //logger.log (r);

      if (r.value < result.value)
       result=r;
    }

   //logger.log (result);
   return result;
 }

}