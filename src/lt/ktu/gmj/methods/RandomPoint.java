// RandomPoint.java

package lt.ktu.gmj.methods;

import lt.ktu.gmj.Domain;
import lt.ktu.gmj.Point;

public class RandomPoint extends Point {

	public RandomPoint(Domain domain) {
		super(domain);

		for (int i = 0; i < x.length; i++)
			x[i] = Math.random();

		domain.normalizePoint(this);
	}

};
