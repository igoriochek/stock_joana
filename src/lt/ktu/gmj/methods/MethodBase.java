// MethodBase.java

package lt.ktu.gmj.methods;

import lt.ktu.gmj.*;

public abstract class MethodBase implements Method {
 public int iterations=1000;
 public int initialPoints=10;
 
 public int initialPoints()
	{
		return initialPoints;
	}

 public int iterations()
 {
   return iterations;
 }
 
};