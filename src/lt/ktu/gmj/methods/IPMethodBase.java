// IPMethodBase.java

package lt.ktu.gmj.methods;

import lt.ktu.gmj.propertySheet.*;

class MethodIPProvider extends SimplePropertyProvider {
 public IPMethodBase method;

 public MethodIPProvider (IPMethodBase _method)
 { method=_method; }

 public Object get ()
  { return new Integer(method.initialPoints); }

 public void set (Object value) throws InvalidPropertyException
  { method.initialPoints=((Integer)value).intValue(); }

};

public abstract class IPMethodBase extends MethodBase {
 public int initialPoints=10;
/*
 public void customize (PropertyManager manager)
 {
   super.customize (manager);
   manager.add ( new IntProperty("Initial points",
                  new MethodIPProvider (this), 1, 1000));
 }*/
 
};