package lt.ktu.gmj.tasks;

import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;

import lt.ktu.gmj.Domain;
import lt.ktu.gmj.Point;
import lt.ktu.gmj.Task;
import defaultPackage.MeGraphics;
import lt.ktu.mockus.srgm.StockContainer;
import defaultPackage.Counter_Sharp;


public class SharpTask implements Task {
	private Domain domain = new Domain();

	int Iterations, InitialPoint;
        double x[];

	double Resours;

	int InType;

	String mMethod;

	Counter_Sharp Counter;

	Vector wItVector, wFVector;

	boolean SuspendStatus = false, StopStatus = false, FrameCreated = false;

	int Risk = 3, it = 0, Length = 0;

	double SumMax = -1000000.0;
        double Ri[][];
        double Ri_[];
        double p[];
        
        double Rp_ = 0;
        StockContainer stocksPrices[];
        int Iter = 0;
        double result[];
        
        
	public SharpTask() {
            System.out.println( "sharp task me constructor " );
	}
        public void setParameters( int Iterations[], int InType, double Resours,
			StockContainer stocksPrices[],  double  result[],  String mMethod, double bankrupt [], double a [] ) {
                System.out.println( "Sharp task set parameters" );
		this.Iterations = Iterations[0];
		this.InitialPoint = Iterations[1];
		this.InType = InType;
		this.Resours = Resours;
                this.stocksPrices = stocksPrices;
		this.mMethod = mMethod;
                Length = stocksPrices.length;
                Iter = stocksPrices[0].getIterNr();
                this.result = result;
	}
        
        void getP() {
           // System.out.println( "getP Sharptask Leghtn = " + Length + " Iter = " + Iter );
            Ri = new double[Length][Iter];
            Ri_ =  new double[Length];
            //int a = 0, i = 0;
            double p = 0;
            for( int i = 0; i < Length; i++ ) {
                for (  int t = 0; t < Iter; t++ ) {
                    if ( t == 0 ) Ri[i][t] = 0;
                    else {
                        p = ( stocksPrices[i].get(t) - stocksPrices[i].get(t-1) ) / stocksPrices[i].get(t-1);
                        Ri[i][t] = p * Resours;
                        System.out.println(" Ri[" + i + "][" + t + "] = " + p + " * " + Resours + " = " + Ri[i][t] );
                    }
                    Ri_[i] += Ri[i][t];
                }
                
            }
            System.out.println( "Ri_ dlina = " + Ri_.length );
            for( int temp = 0; temp < Length ; temp++ ){
                Ri_[temp] = Ri_[temp] / Iter;
                System.out.println( "Ri_[ " + temp + " ] = " + Ri_[temp]  );
            }
        }

	public Domain domain() {
		return domain;
	}

	public void CounterPoint(Thread Counter) {
		this.Counter = (Counter_Sharp)Counter;
	}

	public double f(Point pt) {
		getP();
                x = new double[Length];
		double viso = 0;
		// System.out.println("Length "+pt.x.length);
		for (int i = 0; i < Length; i++)
			viso += pt.x[i];
		if (viso == 0)
			return 0; // data can't be normalize
		if (viso > 1)
			for (int i = 0; i < Length; i++)
				x[i] = pt.x[i] / viso; // normalize
		else
			for (int i = 0; i < Length; i++)
				x[i] = pt.x[i];

		++it;
		
                this.getP();
                
                double Sum = 0.0;
		double disepersija = 0; double Rp = 0;
                
                for ( int i = 0; i < Length; i++ ) {
                    for ( int t = 0; t < Iter; t++ ) {
                        Rp += x[i] * Ri[i][t];
                        //System.out.println( "Rp = " + x[i] + " * " + Ri[i][t] );
                        if ( t > 0 && i > 0 ) {
                            disepersija += x[i] * x[i-1]*( Ri[i][t] - Ri_[i] ) * ( Ri[i-1][t] - Ri_[i-1] ) ;
                            /*System.out.println( "disleprsija " + disepersija + " Rp " + x[i] + " * " + x[i-1] + " * (" + Ri[i][t] + " - " + Ri_[i] + 
                                     "  ) * (  " + Ri[i-1][t]+ " - " + Ri_[i-1] + "  ) " );*/
                        }
                    }
                }
                Rp = Rp / Iter;
                disepersija = disepersija / ( Iter - 1 ); 
                          
                if ( disepersija != 0 ) Sum = Rp / Math.sqrt( disepersija );

		if ( Sum > SumMax ) {
			SumMax = Sum;
                        for( int i = 0; i < x.length; i++ ) {
                            //System.out.println( "Koeficieintas " + i + " = " + x[i] );
                            result[i] = x[i];
                        }
			wItVector.addElement(new Integer(it));
			wFVector.addElement(new Double(SumMax));
                        this.Counter.Counter_sum_max = SumMax;
		}
                
		Wait(it); // Thrred Control
		return -1 * SumMax;
	}

	double Round(double value) {
		long w = 100000000;
		return (double) Math.round(value * w) / w;
	}

	public void Data(Vector wItVector, Vector wFVector) {
		this.wItVector = wItVector;
		this.wFVector = wFVector;
	}

	void Wait(int i) {
		try {
			Thread.sleep(10); // Pause
		} catch (InterruptedException ie) {
		};
		if (StopStatus) {
			//Finished();
			Counter.stop();
		}
		if ((SuspendStatus) && (Iterations != i))
			Suspend();
	}

	void Finished() // Inside thread
	{
            System.out.println( " Sharp counter finished  " );
             System.out.println( "Nauja suma  = " + SumMax );
             System.out.println( "Iteracijos = " + it );
                for( int i = 0; i < x.length; i++ ) {
                    result[i] = x[i];
                    System.out.println( "Koeficieintas " + i + " = " + x[i] );
                }
	}

	public void FinishedCounter() // Outside Thread
	{
		if (SuspendStatus) {
			//Finished();
			Counter.stop();
		}
		StopStatus = true;
	}

	public void StartedCounter() {
	}

	public void Suspend() {
		Counter.suspend();
	}

	public void SuspendCounter() {
		SuspendStatus = true;
	}

	public void ResumeCounter() {
		SuspendStatus = false;
		Counter.resume();
	}
        public double getSumMax() {
            return SumMax;
        }
}
