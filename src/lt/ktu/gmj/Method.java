// Method.java

package lt.ktu.gmj;

public interface Method extends Customizable {

	int iterations();

	void setParameters(int _iterations, int _initialPoints);

	// void setParameters(int _iterations);
	Result run(ResultLogger l, Task t);
};