// Result.java

package lt.ktu.gmj;

public final class Result {
	public int iteration;

	public Point point;

	public double value;

	public Result() {
		iteration = 0;
		point = null;
		value = Double.MAX_VALUE;
		point = null;
	}

	public Result(int _iteration, Point _point, double _value) {
		iteration = _iteration;
		point = _point;
		value = _value;
	}

};