// PropertySheet.Java

package lt.ktu.gmj.propertySheet;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;
import java.util.Vector;

public class PropertySheet extends Panel implements PropertyManager {

	private static final long serialVersionUID = 256672959457995080L;

	protected Vector properties = new Vector();

	GridBagLayout bag = new GridBagLayout();

	public PropertySheet() {
		setLayout(bag);
	}

	public void removeAll() {
		// properties.removeAllElements ();
		super.removeAll();
	}

	public void add(Property property) {
		GridBagConstraints bagC = new GridBagConstraints();
		bagC.insets = new Insets(3, 3, 2, 2);
		bagC.gridy = properties.size();

		Component label = property.label();
		bagC.weightx = 0;
		bagC.fill = GridBagConstraints.HORIZONTAL;
		bag.setConstraints(label, bagC);
		add(label);

		Component editor = property.editor().component();
		bagC.weightx = 1;
		bagC.fill = GridBagConstraints.HORIZONTAL;
		bag.setConstraints(editor, bagC);
		add(editor);

		properties.addElement(property);
	}

	public void validate() {
		GridBagConstraints bagC = new GridBagConstraints();
		bagC.gridy = properties.size();

		Label label = new Label();
		bagC.weighty = 1;
		bag.setConstraints(label, bagC);
		add(label);

		super.validate();
	}

	public void complete() {
		// TODO Auto-generated method stub

	}

};