package lt.ktu.mockus.srgm;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class CSVFileReader {
    String fileName;
    InputStreamReader fileURL;
    ArrayList<String> storeValues = new ArrayList<String>();

    public CSVFileReader(String FileName) {
        this.fileName = FileName;
    }
    
    public CSVFileReader(InputStreamReader FileURL){
        this.fileURL = FileURL;
    }
    
    public void ReadFile() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            StringTokenizer st = null;
            while ( (fileName = br.readLine() ) != null ) {
                storeValues.add(fileName);
                st = new StringTokenizer(fileName, ",");
                while ( st.hasMoreTokens() ) {
                    st.nextToken();
                }
            }
        }
        catch (FileNotFoundException error) {
            System.out.println("Error: " + error);
        }
        catch (IOException error) {
            System.out.println("Error: " + error);
        }
    }

    public void ReadFileFromURL() {
        try {
            BufferedReader br = new BufferedReader( this.fileURL );
            StringTokenizer st = null;
            while ((fileName = br.readLine()) != null) {
                storeValues.add(fileName);
                st = new StringTokenizer(fileName, ",");
                while(st.hasMoreTokens()) {
                    st.nextToken();
                }
            }
        }
        catch (FileNotFoundException error) {
            System.out.println("Error: " + error);
        }
        catch (IOException error) {
            System.out.println("Error: " + error);
        }
    }
    
    public void setFileName(String newFileName) {
        this.fileName = newFileName;
    }
    
    public String getFileName() {
        return fileName;
    }
    
    public ArrayList getFileValues() {
        return this.storeValues;
    }
    
    public void displayArrayList() {
        for (int x=0; x<this.storeValues.size(); x++) {
            System.out.println(storeValues.get(x));
        }
    }
    
    public ArrayList<String> getArrayList() {
        return this.storeValues;
    }
}