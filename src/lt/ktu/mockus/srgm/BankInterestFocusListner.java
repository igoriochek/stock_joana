
package lt.ktu.mockus.srgm;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author igor
 */
public class BankInterestFocusListner implements FocusListener {
	StartupApplet applet;

	public BankInterestFocusListner(StartupApplet applet) {
		this.applet = applet;
	}

	public void focusLost(FocusEvent e) {
		JTextField textField = (JTextField)e.getComponent();
		applet.bankInterestFieldUpdates();
		//System.out.println ("focus lost" + e.getComponent());
	}

	public void focusGained(FocusEvent e) {
		//System.out.println ("focus gained" + e.getComponent());
	}
}
