package lt.ktu.mockus.srgm;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

class FirmCountChangeListener implements ItemListener {
	StartupApplet applet;

	public FirmCountChangeListener(StartupApplet applet) {
		this.applet = applet;
	}

	public void itemStateChanged(ItemEvent e) {
		applet.firmCountUpdated( );
	}

	public void focusGained(FocusEvent e) {}
}
