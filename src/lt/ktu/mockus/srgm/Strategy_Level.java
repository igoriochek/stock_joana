/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ktu.mockus.srgm;

/**
 *
 * @author igor
 */
public class Strategy_Level extends AbstractStrategy {
    
    protected SRGMCustomer customer;
    protected double p;
    protected Strategy_Stock_Pack buyStocks;
    protected Strategy_Stock_Pack sellStocks;
    
    public Strategy_Level( SRGMCustomer customer  ) {
        this.customer = customer;
    }
    

    @Override
    public Strategy_Stock_Pack Buy() {
        return buyStocks;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Strategy_Stock_Pack Sell() {
        return sellStocks;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    
}
