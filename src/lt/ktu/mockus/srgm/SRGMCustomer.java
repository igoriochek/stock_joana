package lt.ktu.mockus.srgm;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SRGMCustomer {
	public StockContainer dZpred  = null;//new double[StartupApplet.MAX_STOCK_COUNT];
	public double dProfit = 0;
        public StockContainer currentPrice = null;
        public ArrayList iN = null;
	public int iIterN = 0;
	public int iStrategy = 0;
        public int riskStrategy = 0;
	public double Delta  = 0;
        
        public int InvestmentType = 0;
        public int InvestmentForShortStrategies = 0;
        public int InvestmentInterval = 1;
        public int InvestmentMetod = 0;
        public int InvestmentIterations = 1000;
        //customers own funds
        public double C0_ = 500;
        public double C0 = C0_;
        //customers kredit limit
        public double Limit = 100;
        //borrowed sum
        public double B = 0;
        public boolean insolvement = false;
        public double forcebuy = 1.0d;
        public double forcesell = 1.0d;
        public boolean insolvementCounted = false;
        
        public int stockCount = 3;
        
        public boolean longInvest = false;
        public int longInvestmentIteration = 0;
        boolean trace = true;
        
        //public int RiskStrategy = 1;
                
        public SRGMCustomer( int stockC ) {
            stockCount = stockC;
        }
        
        public void BuyStocks( int stockIndex, int nb, double price ) {
            int stockC = ((Integer)iN.get(stockIndex)).intValue() + nb;
            iN.set( stockIndex, stockC );
                        
            //cia perkam uz visus pinigus, todel pirmas netinka
            if ( nb <=  C0 / price ) {
                C0 -= nb * price;
            }
            else {
                double sum = nb * price;
                sum -= C0;
                C0 = 0;
                B += sum;
            }
        }
        
        public void SellStocks( int stockIndex, int ns, double price ) {
            int stockC = ((Integer)iN.get(stockIndex)).intValue();
            if ( stockC < ns  ) try {
                throw new Exception( "Customer: norim parduoti daugiau akciju nei turim, turim: " + stockC + " norim parduot " + ns );
            } catch (Exception ex) {
                Logger.getLogger(SRGMCustomer.class.getName()).log(Level.SEVERE, null, ex);
            }
            //System.out.println( "Customer: Parduodam " + stockC + " akciju uz kaina " + price );
            C0 += price * ns ;
            //System.out.println( "Customer: Turim " + C0 + " pinigu " );
            stockC = stockC - ns;
            iN.set( stockIndex, stockC );
        }
        
        public void SellAllStocksByIndex( int stockIndex, double price ) {
            
            C0 += price * ((Integer)iN.get(stockIndex)).intValue();
            iN.set(stockIndex, 0);
        }
        
        public void CheckInsolvement() {
            if ( insolvementCounted ) {
                insolvement = true;
            }
            else {
                double stockMoney = 0;
                for ( int i = 0; i < currentPrice.size(); i++ ) {
                    int in = ((Integer)iN.get(i)).intValue();
                    stockMoney += in * ((Double)currentPrice.get(i)).doubleValue();
                }
                insolvement = ( Limit <= ( B - stockMoney - C0 ) );
            }
        }
        
        public double PayInsolvement() {
            if ( ! insolvement ) return 0;
            double stockMoney = 0;
            for ( int i = 0; i < currentPrice.size(); i++ ) {
                int in = ((Integer)iN.get(i)).intValue();
                double curPr  = ((Double)currentPrice.get(i)).doubleValue();
                stockMoney += in * curPr;
                //stockMoney += iN[i] * currentPrice[i];
            }
            double rez = B - ( stockMoney );
            B = 0;
            Limit = 0;
            C0 = 0;
            for ( int i = 0; i < iN.size(); i++ ) {
                iN.set(i, 0);
            }
            insolvementCounted = true;
            return rez;
        }
        
        public double BankMinus() {
            return B;
        }
        
	public double getAvFunds() {
            CheckInsolvement();
            if ( insolvement ) return 0;
            //System.out.println( " avaliable funds C = " + C0 + " + " + " (  " + Limit + " - " + B + " ) " );
            double rezult = C0 + ( Limit - B );
            if ( rezult >= 0 ) return rezult;
            else return 0;
        }         

        public boolean PayCredit() {
            CheckInsolvement();
            if ( C0 <= 0 || B <= 0  )  return true;
            if (  C0 > B ) {
                C0 = C0 - B;
                B = 0;
                return true;
            }
            else {
                B = B - C0;
                C0 = 0;;
                return true;
            }
        }
                
	public void prepareStorage() {
            iN = new ArrayList();
            currentPrice = new StockContainer();
            dZpred = new StockContainer();
            for( int c = 0; c < stockCount; c++ ) {
                dZpred.add(c, 0);
                iN.add( c, 0 );
                currentPrice.add( c, 0.0 );
            }
            
            
	}
           public void setInitialData( double dZpred[], double dProfit, int iN[], int iIterN, double Delta, double limitCoff ) {
            for( int c = 0; c < stockCount; c++ ) {
                this.dZpred.add(c, dZpred[c]);
                this.iN.add(c, iN[c]);
            }
            this.iIterN = iIterN;
            this.dProfit = dProfit;
            this.Delta = Delta;
            this.longInvest = false;
            this.Limit = this.Limit * limitCoff;
	}
        
        @Override
	public String toString(){
            return "dZpred:" + this.dZpred + " dProfit:" + this.dProfit + " iN:" + this.iN + 
                    " iIterN:" + this.iIterN + " iStrategy:" + this.iStrategy + "riskStrategy:" + this.riskStrategy;
	}
}
