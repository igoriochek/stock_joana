/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author igoriok
 */
package lt.ktu.mockus.srgm;
import javax.swing.*;
import java.awt.event.*;
class CustomerPricesVisibilityChangeListener implements ActionListener
{
	StartupApplet applet;

	public CustomerPricesVisibilityChangeListener(StartupApplet applet)
	{
		this.applet = applet;
	}

	public void actionPerformed(ActionEvent e)
	{
		JCheckBox checkbox = (JCheckBox)e.getSource();
		applet.customerPricesVisibilityChanged( checkbox );
	}
}
