/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lt.ktu.mockus.srgm;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author igoriok
 */
class CustomerProfitVisibilityChangeListener implements ActionListener
{
	StartupApplet applet;

	public CustomerProfitVisibilityChangeListener(StartupApplet applet)
	{
		this.applet = applet;
	}

	public void actionPerformed(ActionEvent e)
	{
		JCheckBox checkbox = (JCheckBox)e.getSource();
		applet.customerProfitVisibilityChanged( checkbox );
	}
}
