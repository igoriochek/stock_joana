/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lt.ktu.mockus.srgm;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import java.awt.event.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

/**
 *
 * @author igoriok
 */
class ControlButtonsListener implements ActionListener
{
	StartupApplet applet;

	public ControlButtonsListener(StartupApplet applet){
		this.applet = applet;
	}

	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton)e.getSource();
            try {
            try {
                applet.controlButtonsPressed(button);
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(ControlButtonsListener.class.getName()).log(Level.SEVERE, null, ex);
            } catch (TransformerConfigurationException ex) {
                Logger.getLogger(ControlButtonsListener.class.getName()).log(Level.SEVERE, null, ex);
            } catch (TransformerException ex) {
                Logger.getLogger(ControlButtonsListener.class.getName()).log(Level.SEVERE, null, ex);
            }
            } 
            catch (IOException ex) {
                ex.printStackTrace();
            }
	}
}
