/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ktu.mockus.srgm;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

public class AppletFrame extends JFrame implements ActionListener {
  public static void main(String[] args) {  
    StartupApplet myApplet = new StartupApplet(); // define applet of interest
    
    
    Frame myFrame = new Frame("Stock Exchange Model"); // create frame with title

    // Call applet's init method (since Java App does not
    // call it as a browser automatically does)
    myApplet.init();	

    // add applet to the frame
    myFrame.add(myApplet, BorderLayout.CENTER);
    myFrame.pack(); // set window to appropriate size (for its elements)
    myFrame.setVisible(true); // usual step to make frame visible

  } // end main
  
    public AppletFrame() { // constructor
    super("Stock Exchange Model"); // define frame title

    // define Menubar
    MenuBar mb = new MenuBar();
    setMenuBar(mb);

    // Define File menu and with Exit menu item
    Menu fileMenu = new Menu("File");
    mb.add(fileMenu);
    MenuItem exitMenuItem = new MenuItem("Exit");
    fileMenu.add(exitMenuItem);
    exitMenuItem.addActionListener (this);

    // define the applet and add to the frame
    StartupApplet myApplet = new StartupApplet();
    
    //JScrollPane scrollPane = new JScrollPane(myApplet);
    //setPreferredSize(new Dimension(800, 600));
    //add(scrollPane, BorderLayout.CENTER);
    
    add(myApplet, BorderLayout.CENTER);

    // call applet's init method (since it is not
    // automatically called in a Java application)
    myApplet.init();

  } // end constructor

  public void actionPerformed(ActionEvent evt) {

    if (evt.getSource() instanceof MenuItem) {
      String menuLabel = ((MenuItem)evt.getSource()).getLabel();

      if(menuLabel.equals("Exit")) {
        // close application, when exit is selected
        dispose();
        System.exit(0);
      } // end if
    } // end if
  } // end ActionPerformed

  
} // end class