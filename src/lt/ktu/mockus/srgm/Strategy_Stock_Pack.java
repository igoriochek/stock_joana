/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ktu.mockus.srgm;
import java.util.LinkedHashMap;


/**
 *
 * @author igor
 */
public class Strategy_Stock_Pack extends LinkedHashMap {
    
    public Strategy_Stock_Pack() {
        super();
    }
    
    public void add(int key, int value){
	super.put(new Integer(key), new Integer(value));
    }
    
    public int get(int key) {
        if ( super.containsKey(key) ) {
            if (super.get(new Integer(key)) != null){
               return ((Integer)super.get(new Integer(key))).intValue();
            }
        }
        return 0;
    }
    
    public void set(int key, int value){
        super.remove(new Integer(key));
        this.add(key, value);
    }
    
    public void unset( int key ) {
        super.remove(new Integer(key));
    }
    
}
