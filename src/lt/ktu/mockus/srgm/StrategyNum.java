/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ktu.mockus.srgm;

/**
 *
 * @author igor
 */
public class StrategyNum {
    static public int B_BS_S_A_LS_BY_P = 0;
    static public int B_BS_S_A_L = 1;
    static public int B_BS_S_A = 2;
    static public int B_S_BY_PRFT_LVL = 3;
    
    static public int LONG_TERM_SHARP = 4;
    static public int LONG_TERM_PORTF = 5;
    
    static public int LONG_B_BS_S_A_LS_BY_P = 6;
    static public int LONG_B_BS_S_A_L = 7;
    static public int LONG_B_BS_S_A = 8;
    static public int LONG_B_S_BY_PRFT_LVL = 9;
    
    static public int LONG_TERM_SHARP_SH = 10;
    static public int LONG_TERM_PORTF_SH = 11;
    static public int LONG_B_BS_S_A_LS_BY_P_SH = 12;
    static public int LONG_B_BS_S_A_L_SH = 13;    
    static public int LONG_B_BS_S_A_SH = 14;
    static public int LONG_B_S_BY_PRFT_LVL_SH = 15;
    
    static public int TEST_STRATEGY = 16;
    static public int LEVEL_STRATEGY = 17;

    
    
    
    /*
       private final static String[] RISK_STRATEGY_METHODS = { "B_BS_S_A_LS_BY_P", "B_BS_S_A_L", "B_BS_S_A" , "B_S_BY_PRFT_LVL",  
                                                                //0                     1           2               3
            "LONG_TERM_SHARP", "LONG_TERM_PORTF" , "LONG_B_BS_S_A_LS_BY_P", "LONG_B_BS_S_A_L", "LONG_B_BS_S_A", "LONG_B_S_BY_PRFT_LVL", 
            //4                     5                   6                       7                   8               9
            "LONG_TERM_SHARP_SH", "LONG_TERM_PORTF_SH", "LONG_B_BS_S_A_LS_BY_P_SH", "LONG_B_BS_S_A_L_SH", "LONG_B_BS_S_A_SH", "LONG_B_S_BY_PRFT_LVL_SH", 
            //10                        11                      12                          13                      14                  15
            "TEST_STRATEGY", "LEVEL_STRATEGY" };
            //16            17
    */
}
