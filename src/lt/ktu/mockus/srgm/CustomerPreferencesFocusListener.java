
package lt.ktu.mockus.srgm;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author igoriok
 */
class CustomerPreferencesFocusListener implements FocusListener {
	StartupApplet applet;
	public CustomerPreferencesFocusListener(StartupApplet applet) {
		this.applet = applet;
	}
	public void focusLost(FocusEvent e) {
		JTextField textField = (JTextField)e.getComponent();
		applet.customerPreferencesUpdated( textField );
	}
	public void focusGained(FocusEvent e) {}
}
