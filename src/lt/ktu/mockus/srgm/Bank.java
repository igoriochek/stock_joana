
package lt.ktu.mockus.srgm;

import org.jfree.data.*;
import org.jfree.data.time.*;
import java.util.*;

public class Bank {
    protected double bankSum = 1000000;
    public ArrayList customers = null;//SRGMCustomer()
    public double bankProfit = 0;
    public double Bsum = 0;
    public TimeSeries profitSeries = null;


    public Bank() {
        profitSeries = new TimeSeries( "bank profit" );
    }

    public void Borrow( double sum ) {
        Bsum += sum;
        bankSum -= sum;
    }

    public void PayCredit( double sum ) {
        Bsum -= sum;
        bankSum += sum;
    }

    public void Paypercent( double sum ) {
        bankProfit += sum;
    }
    
    public void Insolvement( double sum ) {
        bankProfit -= sum;
    }

    public void addClient( SRGMCustomer c ) {
    }

    public void removeClient() {
        
    }

    public void profitKalkulate( int day ) {

    }


}
