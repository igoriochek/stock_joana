package lt.ktu.mockus.srgm;
import javax.swing.*;
import java.awt.event.*;
import java.util.Locale;

/**
 *
 * @author igoriok
 */
class dataSelectorListner implements FocusListener {
	StartupApplet applet;

	public dataSelectorListner(StartupApplet applet) {
		this.applet = applet;
	}

	public void focusLost(FocusEvent e) {
		JComboBox textField = (JComboBox)e.getComponent();
		for ( int ii = 0; ii < applet.MAX_STOCK_COUNT; ii++ ){
                    if ( textField == applet.dataSelector[ii] ) {
                        int i = textField.getSelectedIndex();
                        applet.dataSelector[ii].enable(false);
                        applet.dataSelectorUpdates( i, ii );
                    }
                }
                
                //int i = textField.getSelectedIndex();
                //if ( i == 0 ) 
                //for ( int ii = 0; ii < applet.MAX_STOCK_COUNT; ii++ )
                   // applet.dataSelector[ii].enable(false);
                //else applet.dataSelector.enable(true);
                //applet.dataSelectorUpdates( i );
	}

	public void focusGained(FocusEvent e) {}
}

