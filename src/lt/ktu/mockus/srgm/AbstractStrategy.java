/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ktu.mockus.srgm;

/**
 *
 * @author igor
 */
public abstract class AbstractStrategy {
    
    public abstract Strategy_Stock_Pack Buy();
    public abstract Strategy_Stock_Pack Sell();
    
}
