package lt.ktu.mockus.srgm;

import java.util.LinkedHashMap;

public class StockContainer extends LinkedHashMap {
	
        public StockContainer(){
            super();
	}
	
	public void add(int key, double value){
		super.put(new Integer(key), new Double(value));
	}
	
	//adds to the end
	public void add(double value){
		Integer lastKey = this.getLastKey();
		int newKey = lastKey.intValue() + 1;
		this.add(newKey, value);
	}
	
	public double get(Integer key) {
	// a esli znachenije na samom dele 0???
            if (super.get(key) != null)
		return ((Double)super.get(key)).doubleValue();
            return 0;
	}

	public double get(int key) {
            if (super.get(new Integer(key)) != null)
                    return ((Double)super.get(new Integer(key))).doubleValue();
            return 0;
	}
	
	public Integer getLastKey(){
            Object[] keys = super.keySet().toArray();
            Integer latest = new Integer(0);
            for (int i=0; i<keys.length; i++) {
                Integer key = (Integer)keys[i];
                if (key.compareTo(latest) > 0) 	latest = key;
            }
            return latest;
	}
	
	public int getIterNr(){
		Integer lastKey = getLastKey();
		return lastKey.intValue();
	}
	
	public void set(int key, double value){
		super.remove(new Integer(key));
		this.add(key, value);
	}
	
	public double getLastValue(){
		return this.get(this.getLastKey());
	}
	
	
	public double[] toArray(){
            Object[] values = super.values().toArray();
            double [] result = new double[values.length];
            for (int i=0; i < values.length; i++)
                    result[i] = ((Double)values[i]).doubleValue();
            return result;
	}
        public int[] toArrayAsInt() {
            Object[] values = super.values().toArray();
            int [] result = new int[values.length];
            for ( int i=0; i < values.length; i++ )
                    result[i] = ((Integer)values[i]).intValue();
            return result;
        }
        
        public double[] toArrayRibojimas( int ribojimas ) {
            return this.toArray();
        }
        
        public int count() {
            return this.count();
        }
        
        public double avg() {
            double [] ar = this.toArray();
            int sum = 0;
            for( int i = 0 ; i < ar.length; i++ ) sum+=ar[i];
            return sum / ar.length;
        }

        //igoriok
        public double[] toArrayLastVals( int from ) {
            Object[] values = super.values().toArray();
            double [] result = new double[values.length];
            for (int i= values.length - from; i<values.length; i++)
                    result[i] = ((Double)values[i]).doubleValue();
            return result;
        }
	
    @Override
	public String toString(){
            StringBuilder sb = new StringBuilder();
            Object[] values = super.values().toArray();
            for (int i=0; i< values.length; i++){
                    //sb.append(" [key:"); sb.append(i);
                    //sb.append(" value:"); sb.append(this.get(i)); sb.append("] ");
                    sb.append(this.get(i)); sb.append("\n");		
            }
            return sb.toString();
	}
}
