package lt.ktu.mockus.srgm;
import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author igoriok
 */

class YieldFocusListener implements FocusListener {
	StartupApplet applet;

	public YieldFocusListener(StartupApplet applet) {
		this.applet = applet;
	}

	public void focusLost(FocusEvent e) {
		JTextField textField = (JTextField)e.getComponent();
		applet.yieldFieldUpdated();
	}

	public void focusGained(FocusEvent e) {
	}
}
