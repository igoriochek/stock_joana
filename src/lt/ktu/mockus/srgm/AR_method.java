package lt.ktu.mockus.srgm;

import Jama.Matrix;

public class AR_method {
    private double data[];
    private int p;
    private int m;
    private int n;
    private double det;

    private double A[][];
    private double B[][];
    private double X[];

    public AR_method( double [] v, int p  ) {
        this.p = p;
        this.data = v;
        this.n = v.length;
        this.m = this.n - this.p;
        //printData();
    }

    public void A_() {
        A = new double[p][p];
        for ( int i = 0; i < p; i++ ) {
            for ( int j = 0; j < p; j++ ) {
               for ( int k = data.length-1; k > data.length-1-m; k-- ) {
                   A[i][j] += data[k-j-1] * data[k-i-1];
               }
            }
        }
        //printA();
    }

    public void B_() {
        B = new double[p][1];
        for ( int i = 0; i < p; i++ ) {
           for ( int j = data.length-1; j > data.length-1-m; j-- ) {
               B[i][0] += data[j] * data[j-i-1];
           }
        }
        //printB();
    }

    public double[] rezult() {
        A_();
        B_();
        Matrix A_matrix = new Matrix(A);
        det = A_matrix.det();
        //System.out.println( "det A = " + det );
        Matrix b_matrix = new Matrix(B);
        while ( det < 0.050 ) {
            p = p - 1;
            A_();
            B_();
            A_matrix = new Matrix(A);
            det = A_matrix.det();
            //System.out.println( "det A = " + det );
            b_matrix = new Matrix(B);
        }
        Matrix x_matrix = A_matrix.solve(b_matrix);
        double [][] temp = x_matrix.getArrayCopy();
        X = new double[p];
        for( int i = 0; i < temp.length; i++ )
            X[i] = temp[i][0];


        return X;
    }

     public void printA() {
        String s;
        double temp2;
        for( int i = 0; i < p; i++ ){
            s = "";
            for( int j = 0; j < p; j++ ){
                temp2 = Math.round( A[i][j] * 100 );
                temp2 = temp2 / 100;
                s +=  temp2  + " ";
            }
        }
     }

}