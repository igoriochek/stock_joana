/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ktu.mockus.srgm;

/**
 *
 * @author igor
 */
public class Strategy_BuyBest_Sell_All_By_Levels extends AbstractStrategy {
    
    protected SRGMCustomer customer;
    protected double p[];
    protected double tau[];
    protected double dividend[];
    protected double yield;
    protected double price[];
    protected Strategy_Stock_Pack buyStocks;
    protected Strategy_Stock_Pack sellStocks;
    protected int MAX_STOCK_COUNT;
    
    //poka tak 
    protected double zs = 0;
    
    public Strategy_BuyBest_Sell_All_By_Levels( SRGMCustomer customer, int maxStockCount, double dividend[], double yield, double p[], double tau[], double[] price  ) {
        this.customer = customer;
        this.MAX_STOCK_COUNT = maxStockCount;
        this.p = p;//new double[maxStockCount];
        this.tau = tau;// = new double[maxStockCount];
        this.dividend = dividend;
        this.yield= yield;
        this.price = price;
        sellStocks = new Strategy_Stock_Pack();
    }

    /*Strategy_BuyBest_Sell_All_By_Levels(SRGMCustomer customer, int MAX_STOCK_COUNT, double[] dividend, double yieldOfCd, double[] p, double[] tau) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
    

    @Override
    public Strategy_Stock_Pack Buy() {
        
        
        
        
        return buyStocks;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Strategy_Stock_Pack Sell() {
        for (int c = 0; c < this.MAX_STOCK_COUNT; c++ ) {
            // praleidziam pelningiausia, nors ir taip ja butu praleide veliau
            //if ( c == maxIndexP[j] ) continue; //nereikia, nes ji pelninga
            //praleidziam kur reikia laukti, kur nedengia tranazakcijos
            if ( Math.abs( p[c] ) < Math.abs( tau[c]) ) continue;
            //praledziam pelningas
            if ( p[c] >= tau[c] ) continue;
            // skaiciuojam lygius!!!
            
            int ns3 = ((Integer)customer.iN.get(c)).intValue();
            if ( ns3 < 0 ) {
                System.out.println(" Negative stock value: " + ns3 + ", stock index " + c + " Customer " + this.customer.toString()  );
                ns3 = 0;
                customer.iN.set(c, 0);
            }
            if ( ns3 == 0 ) continue;
            
            int ns1 = 1;
            int ns2 = (int)(  ns3 / 2 );
            if ( ns2 < ns1 ) ns2 = ns1;
            
            
            double ps1[] = new double[this.MAX_STOCK_COUNT];
            double ps2[] = new double[this.MAX_STOCK_COUNT];
            double ps3[] = new double[this.MAX_STOCK_COUNT]; 

            double zs1[] = new double[this.MAX_STOCK_COUNT];
            double zs2[] = new double[this.MAX_STOCK_COUNT];
            double zs3[] = new double[this.MAX_STOCK_COUNT];

            ps1[c] = -tau[c] * 1;
            ps2[c] = -tau[c] * 2;
            ps3[c] = -tau[c] * 3;

            zs1[c] = ( customer.dZpred.get(c) ) / ( 1 - ( this.dividend[c] / 365 ) + ( this.yield / 365 ) + ps1[c] );
            zs2[c] = ( customer.dZpred.get(c) ) / ( 1 - ( this.dividend[c] / 365 ) + ( this.yield / 365 ) + ps2[c] );
            zs3[c] = ( customer.dZpred.get(c) ) / ( 1 - ( this.dividend[c] / 365 ) + ( this.yield / 365 ) + ps3[c] );

            //applet.LoggerWrite(" <table width=\"200px\" border=\"1\"> <tr> <td> zs1 </td> <td> zs2 </td> <td> zs3 </td> </tr> <tr> <td> " + zs1[c] + "</td><td> " + zs2[c] + "</td><td>" + zs3[c] + "</td></tr></table>" );

            if (  ( p[c] <= ps3[c] ) &&  ( ((Integer)customer.iN.get(c)).intValue() > 0 ) ) {
                    //applet.LoggerWrite(" p[c] <= ps3[c] && akcijuKiekis[c] > 0 "+ "<br/>");
                    //int ns = ns3;
                    sellStocks.add(c, ns3);
                    
                    zs = zs3[c];
            }
            else if ( p[c] <= ps2[c] && ( ((Integer)customer.iN.get(c)).intValue() > 0 ) ) {
                   // applet.LoggerWrite(" p[c] <= ps2[c] && akcijuKiekis[c] > 0 " + "<br/>");
                    //ns = ns2;
                    sellStocks.add(c, ns2);
                    zs = zs2[c];
            }
            else if ( p[c] <= ps1[c] && ( ((Integer)customer.iN.get(c)).intValue() > 0 ) ) {
                    //applet.LoggerWrite(" p[c] <= ps1[c] && akcijuKiekis[c] > 0 "+ "<br/>");
                    //ns = 1;
                    sellStocks.add(c, 1);
                    zs = zs1[c];
            }
            else {
                //applet.LoggerWrite("<b> p[c] sell else ( no sell ) </b>"+ "<br/>");
                //ns = 0;
                sellStocks.add(c, 0);
            }
        }

            /*if ( ns > 0 && ns <= ((Integer)customer.iN.get(c)).intValue() ) {
                //applet.LoggerWrite( j + " makes sell day " + iIterN );
                //applet.LoggerWrite(j + " sell action - stock count: "  + ns + " of " + c + " stock" + "<br/>");
                sellmadeHistory = sellmadeHistory + " stock " + c + " : " + ns + " ; ";
                customer.SellStocks( c, ns, price[c] );

                if ( maxsell[c] > zs ) maxsell[c] = zs;
                sellmade[c] = true;
            }
            /*else {
                //applet.LoggerWrite(j + " MISTAKE SELL ns > stock count or ns < 0 ; ns = " + ns + "  >  stock count = "  + ((Integer)customer.iN.get(c)).intValue() );
            }*/
        
        
        return sellStocks;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public double getZs() {
        return this.zs;
    } 
    

    
}
