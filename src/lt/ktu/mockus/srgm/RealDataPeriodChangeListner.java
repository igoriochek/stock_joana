package lt.ktu.mockus.srgm;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

class RealDataPeriodChangeListner implements ItemListener {
	StartupApplet applet;

	public RealDataPeriodChangeListner(StartupApplet applet) {
		this.applet = applet;
	}

        @Override
	public void itemStateChanged(ItemEvent e) {
		applet.realDataPeriodUpdated();
	}

	public void focusGained(FocusEvent e) {}
}
