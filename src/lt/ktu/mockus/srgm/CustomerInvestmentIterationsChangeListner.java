/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ktu.mockus.srgm;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author igoriok
 */
public class CustomerInvestmentIterationsChangeListner implements FocusListener
{
	StartupApplet applet;

	public CustomerInvestmentIterationsChangeListner(StartupApplet applet)
	{
		this.applet = applet;
	}

	public void focusLost(FocusEvent e) {
		JTextField textField = (JTextField)e.getComponent();
		applet.customerPreferencesUpdated( textField );
	}
	public void focusGained(FocusEvent e) {}
}