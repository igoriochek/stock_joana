package lt.ktu.mockus.srgm;

import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class loadYahooData {
    private String stock_symbol;
    private int start_year;
    private int start_month;
    private int start_day;
    private int end_year;
    private int end_month;
    private int end_day;
    private String data[][];
    private String period = "d";
    
    public loadYahooData( String stock_symbol, int start_year, int start_month, int start_day,
            int end_year, int end_month, int end_day, String periodData ) {
        this.stock_symbol = stock_symbol;
        this.start_year = start_year;
        this.start_month = start_month;
        this.start_day = start_day;
        this.end_year = end_year;
        this.end_month = end_month;
        this.end_day = end_day;
        this.period = periodData;
    }
    
    public String[][] rezult() {
        try {
            /*URL yahooUrl = new URL("http://ichart.finance.yahoo.com/table.csv?s=" + stock_symbol +
                    "&a=" + (start_month-1) + "&b=" + start_day + "&c=" + start_year +
                    "&d=" + (end_month-1) + "&e=" + end_day + "&f=" + end_year + 
                    "&g=d&ignore=.csv");*/
            URL yahooUrl = new URL("http://ichart.finance.yahoo.com/table.csv?s=" + stock_symbol +
                    "&a=" + start_month + "&b=" + start_day + "&c=" + start_year +
                    "&d=" + end_month + "&e=" + end_day + "&f=" + end_year + 
                    "&g=" + this.period + "&ignore=.csv");
            //System.out.println( yahooUrl );
            URLConnection yc = yahooUrl.openConnection();
            CSVFileReader csv = new CSVFileReader(new InputStreamReader(yc.getInputStream()));
            csv.ReadFileFromURL();
            ArrayList<String> a = csv.getArrayList();
            data = new String[a.size()-1][7]; //duomenys
            for (int i = 1; i < a.size(); i++)
                data[i-1] = a.get(i).split(",");
        }
        catch(Exception ex) {
            System.out.println("Unable to get stockinfo: " + ex);
        }
        
        System.out.println( "Count data length in load Yahoo data " + data.length );
        return data;
    }
}