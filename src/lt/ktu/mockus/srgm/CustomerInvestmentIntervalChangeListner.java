/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ktu.mockus.srgm;

import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JComboBox;

/**
 *
 * @author igoriok
 */
public class CustomerInvestmentIntervalChangeListner implements ItemListener
{
	StartupApplet applet;

	public CustomerInvestmentIntervalChangeListner(StartupApplet applet)
	{
		this.applet = applet;
	}

	public void itemStateChanged(ItemEvent e)
	{
		JComboBox combo = (JComboBox)e.getItemSelectable();
		applet.customerInvestmentIntervalUpdated(combo);
	}

	public void focusGained(FocusEvent e) {}
}