package lt.ktu.mockus.srgm;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

class StockCountChangeListener implements ItemListener {
	StartupApplet applet;

	public StockCountChangeListener(StartupApplet applet) {
		this.applet = applet;
	}

	public void itemStateChanged(ItemEvent e) {
		applet.stockCountUpdated();
	}

	public void focusGained(FocusEvent e) {}
}
