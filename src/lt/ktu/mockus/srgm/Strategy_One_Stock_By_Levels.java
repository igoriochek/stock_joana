/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.ktu.mockus.srgm;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author igor
 */
public class Strategy_One_Stock_By_Levels extends AbstractStrategy {
    
    protected SRGMCustomer customer;
    protected double p[];
    protected double tau[];
    protected double dividend[];
    protected double yield;
    protected double price[];
    protected Strategy_Stock_Pack buyStocks;
    protected Strategy_Stock_Pack sellStocks;
    protected int MAX_STOCK_COUNT;
    
    //poka tak 
    protected double zs = 0;
    protected double zb = 0;
    
    boolean trace = true;
    
    public Strategy_One_Stock_By_Levels( SRGMCustomer customer, int maxStockCount, double dividend[], double yield, double p[], double tau[], double price[]  ) {
        this.customer = customer;
        this.MAX_STOCK_COUNT = maxStockCount;
        this.p = p;//new double[maxStockCount];
        this.tau = tau;// = new double[maxStockCount];
        this.dividend = dividend;
        this.yield = yield;
        this.price = price;
        sellStocks = new Strategy_Stock_Pack();
        buyStocks = new Strategy_Stock_Pack();
    }
    

    @Override
    public Strategy_Stock_Pack Buy() {
        
        if ( MAX_STOCK_COUNT > 1 ) { try {
            throw new Exception("daugiau nei viena akcija");
            } catch (Exception ex) {
                Logger.getLogger(Strategy_One_Stock_By_Levels.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
            int c = 0;
            if ( Math.abs( p[c] ) < Math.abs( tau[c]) ) {
                buyStocks.add(c, 0);
                if ( trace ) System.out.println( "Neverta prikti nedengia tranazakcijos" );
                return buyStocks;
            }
            //praledziam nuostlingas
            if ( p[c] <= tau[c] ) {
                buyStocks.add(c, 0);
                if ( trace ) System.out.println( "Neverta prikti nuostolinga" );
                return buyStocks;
            }
            // skaiciuojam lygius!!!
            if ( trace ) System.out.println( "Skaiciuojam kiek pirkti, pinigai " + customer.getAvFunds());
            int nb3 = (int) (customer.getAvFunds() / price[c]);
            if ( nb3 > ( (int) (customer.getAvFunds() / price[c]) ) ) nb3  = nb3 - 1;
            if ( trace ) System.out.println( "Maksimalus galimas kiekis: " + nb3 );
            if ( nb3 < 0 ) {
                if ( trace ) System.out.println(" Negative avaliable funds value: " + nb3 + " Customer " + this.customer.toString()  );
                nb3 = 0;
            }
            
            int nb1 = 1;
            int nb2;
            if ( nb3 == 0 ) {
                if ( trace ) System.out.println(" Negali pirkti, nera pinigu nb3: " + nb3 + " Customer " + this.customer.toString()  );
                buyStocks.add(c, 0);
            }
            else {
                if ( trace ) System.out.println( "Perkam" );
                
                nb2 = (int)(  nb3 / 2 );
                if ( nb2 < nb1 ) nb2 = nb1; 
            
            
                double pb1[] = new double[this.MAX_STOCK_COUNT];
                double pb2[] = new double[this.MAX_STOCK_COUNT];
                double pb3[] = new double[this.MAX_STOCK_COUNT]; 

                double zb1[] = new double[this.MAX_STOCK_COUNT];
                double zb2[] = new double[this.MAX_STOCK_COUNT];
                double zb3[] = new double[this.MAX_STOCK_COUNT];

                pb1[c] = tau[c] * 1;
                pb2[c] = tau[c] * 2;
                pb3[c] = tau[c] * 3;

                zb1[c] = ( customer.dZpred.get(c) ) / ( 1 - ( this.dividend[c] / 365 ) + ( this.yield / 365 ) + pb1[c] );
                zb2[c] = ( customer.dZpred.get(c) ) / ( 1 - ( this.dividend[c] / 365 ) + ( this.yield / 365 ) + pb2[c] );
                zb3[c] = ( customer.dZpred.get(c) ) / ( 1 - ( this.dividend[c] / 365 ) + ( this.yield / 365 ) + pb3[c] );
                
                if ( trace ) System.out.println( " pc = " + p[c] + "zb 1,2,3 " + pb1[c] + " " + pb2[c] + " " + pb3[c] );
                
                if (  p[c] >= pb3[c] ) {
                        //applet.LoggerWrite(" p[c] <= ps3[c] && akcijuKiekis[c] > 0 "+ "<br/>");
                        //int ns = ns3;
                        if ( trace ) System.out.println(" Perkam nb3 : " + nb3   );
                        buyStocks.add(c, nb3);
                        zb = zb3[c];
                }
                else if ( p[c] >= pb2[c]  ) {
                       // applet.LoggerWrite(" p[c] <= ps2[c] && akcijuKiekis[c] > 0 " + "<br/>");
                        //ns = ns2;
                        buyStocks.add(c, nb2);
                        if ( trace ) System.out.println(" Perkam nb2 : " + nb2   );
                        zb = zb2[c];
                }
                else if ( p[c] >= pb1[c] ) {
                        //applet.LoggerWrite(" p[c] <= ps1[c] && akcijuKiekis[c] > 0 "+ "<br/>");
                        //ns = 1;
                        buyStocks.add(c, 1);
                        if ( trace ) System.out.println(" Perkam nb1 : " + nb1   );
                        zb = zb1[c];
                }
                else {
                    //applet.LoggerWrite("<b> p[c] sell else ( no sell ) </b>"+ "<br/>");
                    //ns = 0;
                    System.out.println(" Perkam nuli, nors galim daugiau : "  );
                    buyStocks.add(c, 0);
                }
            }
            
            
            if ( trace ) System.out.println( " " );
            if ( trace ) System.out.println( "------------------------------------------------------ " );
            if ( trace ) System.out.println( " " );
        
        return buyStocks;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
        public Strategy_Stock_Pack Sell() {
        //for (int c = 0; c < this.MAX_STOCK_COUNT; c++ ) {
            // praleidziam pelningiausia, nors ir taip ja butu praleide veliau
            //if ( c == maxIndexP[j] ) continue; //nereikia, nes ji pelninga
            //praleidziam kur reikia laukti, kur nedengia tranazakcijos
            int c = 0;
            if ( Math.abs( p[c] ) < Math.abs( tau[c]) ) {
                sellStocks.add(c, 0);
                if ( trace ) System.out.println( "Neverta parduoti nedengia tranazakcijos" );
                return sellStocks;
            }
            //praledziam pelningas
            if ( p[c] >= tau[c] ) {
                sellStocks.add(c, 0);
                if ( trace ) System.out.println( "Neverta parduoti pelniga" );
                return sellStocks;
            }
            // skaiciuojam lygius!!!
            
            int ns3 = ((Integer)customer.iN.get(c)).intValue();
            if ( trace ) System.out.println(" Turim akciju : " + ns3 );
            if ( ns3 < 0 ) {
                if ( trace ) System.out.println(" Negative stock value: " + ns3 + ", stock index " + c + " Customer " + this.customer.toString()  );
                ns3 = 0;
                customer.iN.set(c, 0);
            }
            if ( ns3 == 0 ) {
                sellStocks.add(c, 0);
                if ( trace ) System.out.println( "Neturim akciju, nera ka parduoti" );
                return sellStocks;
            }
            
            int ns1 = 1;
            int ns2 = (int)(  ns3 / 2 );
            if ( ns2 < ns1 ) ns2 = ns1;
            
            
            double ps1[] = new double[this.MAX_STOCK_COUNT];
            double ps2[] = new double[this.MAX_STOCK_COUNT];
            double ps3[] = new double[this.MAX_STOCK_COUNT]; 

            double zs1[] = new double[this.MAX_STOCK_COUNT];
            double zs2[] = new double[this.MAX_STOCK_COUNT];
            double zs3[] = new double[this.MAX_STOCK_COUNT];

            ps1[c] = -tau[c] * 1;
            ps2[c] = -tau[c] * 2;
            ps3[c] = -tau[c] * 3;

            zs1[c] = ( customer.dZpred.get(c) ) / ( 1 - ( this.dividend[c] / 365 ) + ( this.yield / 365 ) + ps1[c] );
            zs2[c] = ( customer.dZpred.get(c) ) / ( 1 - ( this.dividend[c] / 365 ) + ( this.yield / 365 ) + ps2[c] );
            zs3[c] = ( customer.dZpred.get(c) ) / ( 1 - ( this.dividend[c] / 365 ) + ( this.yield / 365 ) + ps3[c] );

            //applet.LoggerWrite(" <table width=\"200px\" border=\"1\"> <tr> <td> zs1 </td> <td> zs2 </td> <td> zs3 </td> </tr> <tr> <td> " + zs1[c] + "</td><td> " + zs2[c] + "</td><td>" + zs3[c] + "</td></tr></table>" );

            if (  ( p[c] <= ps3[c] ) &&  ( ((Integer)customer.iN.get(c)).intValue() > 0 ) ) {
                    //applet.LoggerWrite(" p[c] <= ps3[c] && akcijuKiekis[c] > 0 "+ "<br/>");
                    //int ns = ns3;
                    if ( trace ) System.out.println("Parduodam ns3 " + ns3 );
                    sellStocks.add(c, ns3);
                    
                    zs = zs3[c];
            }
            else if ( p[c] <= ps2[c] && ( ((Integer)customer.iN.get(c)).intValue() > 0 ) ) {
                   // applet.LoggerWrite(" p[c] <= ps2[c] && akcijuKiekis[c] > 0 " + "<br/>");
                    if ( trace ) System.out.println("Parduodam ns2 " + ns2 );
                    sellStocks.add(c, ns2);
                    zs = zs2[c];
            }
            else if ( p[c] <= ps1[c] && ( ((Integer)customer.iN.get(c)).intValue() > 0 ) ) {
                    //applet.LoggerWrite(" p[c] <= ps1[c] && akcijuKiekis[c] > 0 "+ "<br/>");
                    if ( trace ) System.out.println("Parduodam ns1 " + ns1 );
                    sellStocks.add(c, 1);
                    zs = zs1[c];
            }
            else {
                //applet.LoggerWrite("<b> p[c] sell else ( no sell ) </b>"+ "<br/>");
                //ns = 0;
                if ( trace ) System.out.println("Parduodam 0, nors turim daugiau " );
                sellStocks.add(c, 0);
            }
        //}
        
        if ( trace ) System.out.println( " " );
        if ( trace ) System.out.println( "------------------------------------------------------ " );
        if ( trace ) System.out.println( " " );
        return sellStocks;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public double getZs() {
        return this.zs;
    } 
    public double getZb() {
        return this.zb;
    } 

    
}

