/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lt.ktu.mockus.srgm;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JTextField;

/**
 *
 * @author igoriok
 */
class StockPreferencesFocusListener implements FocusListener {
	StartupApplet applet;

	public StockPreferencesFocusListener(StartupApplet applet) {
		this.applet = applet;
	}

	public void focusLost(FocusEvent e) {
		JTextField textField = (JTextField)e.getComponent();
		applet.stockPreferencesUpdated( textField );
	}

	public void focusGained(FocusEvent e) {}
}
