package lt.ktu.mockus.srgm;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author igor
 */
public class TransactionFocusListner implements FocusListener {
	StartupApplet applet;

	public TransactionFocusListner(StartupApplet applet) {
		this.applet = applet;
	}

	public void focusLost(FocusEvent e) {
		JTextField textField = (JTextField)e.getComponent();
		applet.transactionCostFieldUpdates();
	}

	public void focusGained(FocusEvent e) {
	}
}
