package defaultPackage;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Vector;

import javax.swing.JViewport;

public class MeGraphics extends JViewport {

	private static final long serialVersionUID = 7411826836083709085L;

	int Iterations, X, Y, size;

	Vector wItVector, wFVector;

	Dimension wDimension;

	Graphics g;

	double last = 1;

	public MeGraphics(int Iterations, Vector wItVector, Vector wFVector) {
		this.Iterations = Iterations;
		if (Iterations % 10 != 0)
			this.Iterations = (Iterations + 10) - (Iterations % 10);
		this.wItVector = wItVector;
		this.wFVector = wFVector;
		setOpaque(false);
	}

	public void setIterations(int Iterations) {
		this.Iterations = Iterations;
		if (Iterations % 10 != 0)
			this.Iterations = (Iterations + 10) - (Iterations % 10);
	}

	public void paint(Graphics g) {
		this.g = g;
		wDimension = new Dimension(getSize());
		if (Iterations < 10)
			Iterations = 10;
		X = (int) wDimension.getWidth();
		Y = (int) wDimension.getHeight();
		Grid();
		F();
	}

	public void F() // Drawing Function
	{
		g.setColor(Color.red);
		int By = 30, Bx = 30;
		int Xx = X - Bx * 2;
		int Yy = Y - By * 2;
		int x = 0, y = 0, xs, ys;
		for (int i = 0; i <= size; i++) {
			Integer it = (Integer) wItVector.elementAt(i);
			Double f = (Double) wFVector.elementAt(i);
			xs = x;
			ys = y;
			x = it.intValue() * Xx / Iterations;
			y = (int) (f.doubleValue() * Yy / last);
			g.drawLine(Bx + xs, Y - By - ys, Bx + x, Y - By - y);
			g
					.drawLine(Bx + xs - 1, Y - By - ys - 1, Bx + x - 1, Y - By
							- y - 1);
			g.drawLine(Bx + xs - 1, Y - By - ys, Bx + x - 1, Y - By - y);
			g.drawLine(Bx + xs, Y - By - ys - 1, Bx + x, Y - By - y - 1);
		}
	}

	void Grid() // Drawing Grid
	{
		int By = 30, Bx = 30;
		g.setColor(new Color(255, 255, 255));
		g.fillRect(0, 0, X, Y);
		g.setColor(new Color(64, 72, 144));
		g.drawLine(Bx, Y - By, Bx, By - 2); // Y
		g.drawLine(Bx - 1, Y - By, Bx - 1, By - 2);
		g.drawLine(X - Bx + 2, Y - By, Bx, Y - By); // X
		g.drawLine(X - Bx + 2, Y - By - 1, Bx, Y - By - 1);
		size = wItVector.size() - 1;
		if (size != -1) {
			Double Last = (Double) wFVector.elementAt(size);
			last = Last.doubleValue(); // +Last.doubleValue()/100;
		}
		int j = 1;
		while (j < last)
			++j;
		last = j;
		int StepX = (X - Bx * 2) / 10;
		int StepY = (Y - By * 2) / 10;
		for (int i = 0; i < 10; i++) {
			g.drawLine(X - Bx - StepX * i, By - 2, X - Bx - StepX * i, Y - By
					+ 2); // Y grid
			g.drawLine(Bx - 2, By + StepY * i, X - Bx + 2, By + StepY * i); // X
																			// grid
																			// ciaa
		}
		g.setColor(Color.black); // Text
		g.drawString("It", X - Bx + 10, Y - By);
		g.drawString("U(x)", 5, By - 10);

		g.drawString(("0"), 5, Y - By + 5); // Y text

		for (int i = 1; i <= 10; i++) {
			g.drawString(("" + Round(last / 10 * i, 10000)), 5, Y - By + 1
					- StepY * i);
		}
		g.drawString(("0"), Bx - 5, Y - By + 15); // X text
		for (int i = 1; i <= 10; i++) {
			g.drawString(("" + Iterations / 10 * i), Bx - 10 + StepX * i, Y
					- By + 15);
		}
	}

	double Round(double value, long w) {
		return (double) Math.round(value * w) / w;
	}

}