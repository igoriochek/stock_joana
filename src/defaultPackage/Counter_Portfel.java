package defaultPackage;

import java.util.Vector;
import lt.ktu.gmj.Method;
import lt.ktu.gmj.Task;
import lt.ktu.gmj.tasks.PortfelTask;
import lt.ktu.gmj.ui.ResultSet;
import lt.ktu.mockus.srgm.StockContainer;

public class Counter_Portfel extends Thread {
	int Iterations[], BankLength, ShareLength, InsuranceLength;
        double AllBankrupt[], AllDividends[], x[], ObjectValue[], Uf[];

	double Resours;

	int InType;

	String mMethod;

	Vector wItVector, wFVector;

	int Length;

	Method method;

	Task task;
        
        double rezult[];
        
        StockContainer prices [];
        
        public double Counter_sum_max = -1000000.0;
        
        public double bankrupt[];
        public double profitability[];
       
	public Counter_Portfel(int Iterations[], int InType, double Resours, 
                    String mMethod, StockContainer [] prices, double result [], double bankrupt [], double profitability[] ) {
		this.Iterations = Iterations;
		this.InType = InType;
		this.Resours = Resours;
		this.mMethod = mMethod;
                this.prices = prices;
                this.rezult = result;
                this.Length = prices.length;
                this.bankrupt = bankrupt;
                this.profitability = profitability;
	}
        
	public void run() { 
		mMethod = "lt.ktu.gmj.methods." + mMethod;
		try {
			method = (Method) Class.forName(mMethod).newInstance();
		} catch (Exception exc) {
			System.out.println("Error: class " + mMethod + " not found.");
			return;
		}
		try {
			task = (Task) new PortfelTask();
			task.domain().CreateArray(Length);
			/*task.setParameters(Iterations, InType, Resours, AllBankrupt,
					AllDividends, BankLength, InsuranceLength,
					ObjectValue, mMethod, Uf);*/
                        task.setParameters( Iterations, InType, Resours, prices,  rezult,  mMethod, this.bankrupt, profitability );
			task.CounterPoint(this);
			task.Data(wItVector, wFVector);
		} catch (Exception exc) {
			System.out.println("Error: class lt.ktu.gmj.tasks.PortfelTaks not found.");
			return;
		}
		ResultSet results = new ResultSet(method, task);
		results.newLog();		
                try {
                    sleep(200); // Pause
		} catch (InterruptedException ie) {
                    ie.printStackTrace();
		}
                 // lt.ktu.gmj.methods.Bayes
                if (mMethod.equals("lt.ktu.gmj.methods.Bayes")) {

                    Iterations[1] = 10;
                    System.out.println("Bayes IT[1] = : " + Iterations[1] );
                }

		try {
                    //System.out.println("Iterations 0 : " + Iterations[0]);
                    //System.out.println("Iterations 1 : " + Iterations[1]);
                    method.setParameters(Iterations[0], Iterations[1]);
                    method.run(results, task); // Run method and task
		} catch (Exception exception) {
                    exception.printStackTrace();
		}
	}

	public void Data(Vector wItVector, Vector wFVector) {
		this.wItVector = wItVector;
		this.wFVector = wFVector;
	}	
}
