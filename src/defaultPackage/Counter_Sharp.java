package defaultPackage;

//package lt.ktu.gmj.tasks;

import java.util.Vector;
/*
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;
*/
import lt.ktu.gmj.Method;
import lt.ktu.gmj.Task;
//import lt.ktu.gmj.tasks.MePortfolio;
import lt.ktu.gmj.tasks.SharpTask;
import lt.ktu.gmj.ui.ResultSet;
import lt.ktu.mockus.srgm.StockContainer;

public class Counter_Sharp extends Thread {
	int Iterations[], BankLength, ShareLength, InsuranceLength;
        double AllBankrupt[], AllDividends[], x[], ObjectValue[], Uf[];

	double Resours;

	int InType;

	String mMethod;

	Vector wItVector, wFVector;

	int Length;

	Method method;

	Task task;
        
        double rezult[];
        
        StockContainer prices [];
        
        public double Counter_sum_max = -1000000.0;
       
	public Counter_Sharp(int Iterations[], int InType, double Resours, 
                    String mMethod, StockContainer [] prices, double result [] ) {
		this.Iterations = Iterations;
		this.InType = InType;
		this.Resours = Resours;
		this.mMethod = mMethod;
                this.prices = prices;
                this.rezult = result;
                this.Length = prices.length;
	}

	// ********************************************************************

	public void run() {
		//Length = AllDividends.length;
		
                /*for (int i = 0; i <= Length + 2; i++) {
			wALabel[i].setText("0");
                }
                */ 
		// ----------------------------------------------------
                //System.out.println( "Sharp Counter iterations " + Iterations[0] ); 
		mMethod = "lt.ktu.gmj.methods." + mMethod;
                //System.out.println( "Metod: " + mMethod );
                
		try {
			method = (Method) Class.forName(mMethod).newInstance();
		} catch (Exception exc) {
			System.out.println("Error: class " + mMethod + " not found.");
			return;
		}
		try {
			task = (Task) new SharpTask();
			task.domain().CreateArray(Length);
			/*task.setParameters(Iterations, InType, Resours, AllBankrupt,
					AllDividends, BankLength, InsuranceLength,
					ObjectValue, mMethod, Uf);*/
                        double bankrot[] = new double [prices.length];
                        task.setParameters( Iterations, InType, Resours, 
			prices,  rezult,  mMethod, bankrot, bankrot );
			task.CounterPoint(this);
			task.Data(wItVector, wFVector);
		} catch (Exception exc) {
			System.out.println("Error: class lt.ktu.gmj.tasks.MePortfolio not found.");
			return;
		}
		ResultSet results = new ResultSet(method, task);
		results.newLog();
		/*if (mGraphics != null) {
			int IT;
			if (mMethod.equals("lt.ktu.gmj.methods.Globt"))
				IT = (Iterations[0] * 3) + 1;
			else
				IT = Iterations[0];
                       
			mGraphics.setIterations(IT);
			//FrameC(mGraphics);
		}*/
		//StartedCounter();
		
                try {
                    sleep(200); // Pause
		} catch (InterruptedException ie) {
                    ie.printStackTrace();
		}
                 // lt.ktu.gmj.methods.Bayes
                if (mMethod.equals("lt.ktu.gmj.methods.Bayes")) {

                    Iterations[1] = 10;
                    //System.out.println("Bayes IT[1] = : " + Iterations[1] );
                }
                
                /*if (mMethod.equals("lt.ktu.gmj.methods.Globt"))
				Iterations[0] = (Iterations[0] * 3) + 1;*/
		try {
                    //System.out.println("Iterations 0 : " + Iterations[0]);
                    //System.out.println("Iterations 1 : " + Iterations[1]);
                    method.setParameters(Iterations[0], Iterations[1]);
                    method.run(results, task); // Run method and task
		} catch (Exception exception) {
                    exception.printStackTrace();
		}
                /*for ( int i = 0 ; i < rezult.length ; i++ )
                    System.out.println( " koef " + i + " = " + rezult[i] );*/
		//Finished();
	}
        
       

	// ------------------------------------------

	public void Data(Vector wItVector, Vector wFVector) {
		this.wItVector = wItVector;
		this.wFVector = wFVector;
	}
        /*
	public void FrameD() {
		task.FrameD();
	}*/
/*
	public void FrameC(MeGraphics mGraphics) {
		task.FrameC(mGraphics);
	}
*/
	// -----------------------------------------------------------------
	// -------------- Thread Control -----------------------------------

	/*void Finished() // Inside thread
	{     
           for ( int i = 0 ; i < rezult.length ; i++ )
                System.out.println( " koef " + i + " = " + rezult[i] );
	}
        /*
	public void FinishedCounter() // Outside Thread
	{
		task.FinishedCounter();
	}
        */
        /*
	void StartedCounter() {
	}
        */
        /*
	public void SuspendCounter() // Outside Thread
	{
		task.SuspendCounter();
	}

	public void ResumeCounter() // Outside Thread
	{
		task.ResumeCounter();
	}
        */ 
}
