package test;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class TestDialog extends JDialog {

	private JLabel moneyLabel;

	private JLabel profitLabel;


	private JSpinner spinner;

	private double value;

	private double stepSize = 0.05;

	public static final int CANCEL = 0;

	public static final int OK = 1;

	private int returnValue = CANCEL;

	private int money = 0;

	public TestDialog(double min, int money) {
		super();
		this.money = money;

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				exit();
			}
		});

		//setBounds(100, 100, 244, 176);
		setModal(true);

		final JPanel panel = new JPanel();
		panel.setMinimumSize(new Dimension(200, 0));
		panel.setLayout(new GridBagLayout());
		panel.setBorder(new TitledBorder(new EtchedBorder(), "Option",
				TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, null, null));
		getContentPane().add(panel, BorderLayout.CENTER);

		final JPanel panel_1 = new JPanel();
		panel_1.setMinimumSize(new Dimension(150, 0));
		getContentPane().add(panel_1, BorderLayout.SOUTH);
		setTitle("Utility function");
		//setResizable(false);

		final JButton okButton = new JButton();
		okButton.setPreferredSize(new Dimension(60, 25));
		okButton.setMinimumSize(new Dimension(20, 15));
		okButton.setText("OK");
		panel_1.add(okButton);
		okButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				setReturnValue(OK);
				double d = ((Double) spinner.getValue()).doubleValue();
				setValue(d);
				setMoney(Integer.parseInt(profitLabel.getText()));
				exit();
			}
		});

		final JLabel yourManyLabel = new JLabel();
		yourManyLabel.setMinimumSize(new Dimension(0, 15));
		yourManyLabel.setText("Your money: ");
		final GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.fill = GridBagConstraints.BOTH;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.ipadx = 5;
		gridBagConstraints.ipady = 1;
		gridBagConstraints.insets = new Insets(5, 5, 5, 0);
		panel.add(yourManyLabel, gridBagConstraints);

		moneyLabel = new JLabel();
		
		moneyLabel.setText(money + "");
		final GridBagConstraints gridBagConstraints_3 = new GridBagConstraints();
		gridBagConstraints_3.anchor = GridBagConstraints.NORTHWEST;
		gridBagConstraints_3.weightx = 1;
		gridBagConstraints_3.fill = GridBagConstraints.BOTH;
		gridBagConstraints_3.gridx = 1;
		gridBagConstraints_3.gridy = 0;
		gridBagConstraints_3.insets = new Insets(5, 5, 5, 0);
		panel.add(moneyLabel, gridBagConstraints_3);

		final JLabel moneyAfterInvestmentLabel = new JLabel();
		moneyAfterInvestmentLabel.setMinimumSize(new Dimension(0, 15));
		moneyAfterInvestmentLabel.setText("Money after investment: ");
		final GridBagConstraints gridBagConstraints_1 = new GridBagConstraints();
		gridBagConstraints_1.fill = GridBagConstraints.BOTH;
		gridBagConstraints_1.anchor = GridBagConstraints.NORTHWEST;
		gridBagConstraints_1.weightx = 1;
		gridBagConstraints_1.gridx = 0;
		gridBagConstraints_1.gridy = 1;
		gridBagConstraints_1.ipadx = 5;
		gridBagConstraints_1.ipady = 1;
		gridBagConstraints_1.insets = new Insets(5, 5, 5, 0);
		panel.add(moneyAfterInvestmentLabel, gridBagConstraints_1);

		spinner = new JSpinner();
		spinner.setMinimumSize(new Dimension(50, 17));
		final GridBagConstraints gridBagConstraints_4 = new GridBagConstraints();
		gridBagConstraints_4.weightx = 1;
		gridBagConstraints_4.fill = GridBagConstraints.BOTH;
		gridBagConstraints_4.anchor = GridBagConstraints.NORTHWEST;
		gridBagConstraints_4.gridx = 1;
		gridBagConstraints_4.gridy = 1;
		gridBagConstraints_4.insets = new Insets(5, 5, 5, 5);
		panel.add(spinner, gridBagConstraints_4);
		final SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel();
		spinnerNumberModel.setMinimum(new Double(min));
		spinnerNumberModel.setStepSize(new Double(0.05));
		spinnerNumberModel.setMaximum(new Double(1.00));
		
		
		if ((min + stepSize * 2) <= 1.0)
			spinnerNumberModel.setValue(new Double(min + stepSize * 2));
		else
			spinnerNumberModel.setValue(new Double(min + stepSize));
		spinner.setModel(spinnerNumberModel);
		spinnerNumberModel.addChangeListener(new ChangeListener() {

			public void stateChanged(ChangeEvent arg0) {
				changeProbability();
			}
		});

		final JLabel profitProbabilityLabel = new JLabel();
		
		final GridBagConstraints gridBagConstraints_2 = new GridBagConstraints();
		gridBagConstraints_2.fill = GridBagConstraints.BOTH;
		gridBagConstraints_2.anchor = GridBagConstraints.NORTHWEST;
		gridBagConstraints_2.weightx = 1;
		gridBagConstraints_2.gridx = 0;
		gridBagConstraints_2.gridy = 2;
		gridBagConstraints_2.ipadx = 5;
		gridBagConstraints_2.ipady = 1;
		gridBagConstraints_2.insets = new Insets(5, 5, 5, 0);
		panel.add(profitProbabilityLabel, gridBagConstraints_2);
		profitProbabilityLabel.setText("Profit probability:");

		profitLabel = new JLabel();
		final GridBagConstraints gridBagConstraints_5 = new GridBagConstraints();
		gridBagConstraints_5.ipadx = 26;
		gridBagConstraints_5.weightx = 1;
		gridBagConstraints_5.anchor = GridBagConstraints.NORTHWEST;
		gridBagConstraints_5.fill = GridBagConstraints.BOTH;
		gridBagConstraints_5.gridx = 1;
		gridBagConstraints_5.gridy = 2;
		gridBagConstraints_5.insets = new Insets(5, 5, 5, 0);
		panel.add(profitLabel, gridBagConstraints_5);
		profitLabel.setText("110");

		final JButton cabcelButton = new JButton();
		cabcelButton.setMinimumSize(new Dimension(20, 15));
		cabcelButton.setText("Cancel");
		panel_1.add(cabcelButton);
		cabcelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setReturnValue(CANCEL);
				exit();
			}
		});
		//
		changeProbability();
		pack();		
	}

	/**
	 * Automaticaly changes profit, if probability changed
	 * 
	 */
	protected void changeProbability() {
		setValue(((Double) spinner.getValue()).doubleValue());
		double profit = getMoney() + getValue() * getMoney();
		profitLabel.setText((int) profit + "");
	}

	/**
	 * @return Returns the value.
	 */
	public double getValue() {
		return value;
	}

	/**
	 * @param value
	 *            The value to set.
	 */
	private void setValue(double value) {
		this.value = value;
	}

	/**
	 * 
	 */
	private void exit() {
		dispose();
	}

	/**
	 * @return Returns the returnValue.
	 */
	public int getReturnValue() {
		return returnValue;
	}

	/**
	 * @param returnValue
	 *            The returnValue to set.
	 */
	private void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}

	/**
	 * @return Returns the money.
	 */
	public int getMoney() {
		return money;
	}

	/**
	 * @param money
	 *            The money to set.
	 */
	private void setMoney(int money) {
		this.money = money;
	}

}
