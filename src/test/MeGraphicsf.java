package test;

import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
import javax.swing.event.*;

	
public class MeGraphicsf extends JViewport
{
	int Iterations, X, Y, size;
	Dimension wDimension;
	Graphics g;
	double last = 1, Uf[];
		
	public MeGraphicsf(double Uf[])
		{
			Iterations = UtilityPanel.RIBA;
			this.Uf = Uf;
			setOpaque(false);
		}
			
	public void Data(double Uf[])
	{
		this.Uf = Uf;
	}
				
	public void paint(Graphics g)
	{
		this.g = g;
		setBackground(Color.white);
		wDimension = new Dimension(getSize());
		X = (int)wDimension.getWidth();
		Y = (int)wDimension.getHeight();
		Grid();
		F();
	}
		
		
	public void F() //Drawing Function
	{
		g.setColor(Color.red);
		int By = 30, Bx = 30;
		int Xx = X-Bx*2;
		int Yy = Y-By*2;
		int x = 0, y = 0, xs, ys;
		for (int i=0; i<=Iterations; i++)
		{
			xs = x;
			ys = y;
			x = Xx*i/10;
			y = (int)(Uf[i]*Yy/last);					
			g.drawLine(Bx+xs, Y-By-ys, Bx+x, Y-By-y);
			g.drawLine(Bx+xs-1, Y-By-ys-1, Bx+x-1, Y-By-y-1);
			g.drawLine(Bx+xs-1, Y-By-ys, Bx+x-1, Y-By-y);
			g.drawLine(Bx+xs, Y-By-ys-1, Bx+x, Y-By-y-1);
		} 
	}
		
		
	void Grid() //Drawing Grid
	{
		int By = 30, Bx = 30;
		g.setColor(new Color(255, 255, 255));
		g.fillRect(0, 0, X, Y);
		g.setColor(new Color(64, 72, 144));
		g.drawLine(Bx, Y-By, Bx, By-2); // Y
		g.drawLine(Bx-1, Y-By, Bx-1, By-2);
		g.drawLine(X-Bx+2, Y-By, Bx, Y-By); // X
		g.drawLine(X-Bx+2, Y-By-1, Bx, Y-By-1);
		last = 1;
		int StepX = (X-Bx*2)/10;
		int StepY = (Y-By*2)/10;
		for (int i=0; i<10; i++) 
		{
			if (i < 10)
				g.drawLine(X-Bx-StepX*i, By-2, X-Bx-StepX*i, Y-By+2); //X grid
			g.drawLine(Bx-2, By+StepY*i, X-Bx+2, By+StepY*i); //Y grid 
		}
		g.setColor(Color.black); // Text
		g.drawString("v", X-Bx+10, Y-By);	
		g.drawString("F(x)", 5, By-13);
			
		g.drawString(("0"), 5, Y-By+5); //Y text
				
		for (int i=1; i<=10; i++) 
		{
			g.drawString((""+Round(last/10*i, 10000)), 5, Y-By+1-StepY*i);
		}				
		g.drawString(("0"), Bx-5, Y-By+15); //X text				
		for (int i=1; i<=Iterations; i++) 
		{
			g.drawString((""+i), Bx+StepX*i, Y-By+15);
		}	
	}
		
	double Round(double value, long w)
	{
		return (double)Math.round(value*w)/w;
	}
		
}