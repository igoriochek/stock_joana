package test;

import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.JApplet;
import javax.swing.JOptionPane;

public class Test {
	double tmp[] = new double[UtilityPanel.RIBA + 1];

	int Resource;

	private int profit = 0;

	JApplet wApplet;

	public Test(JApplet wApplet, int Resource) {
		this.wApplet = wApplet;
		this.Resource = Resource;
		this.profit = Resource;
	}

	/**
	 * 
	 * @return
	 */
	public double[] Run1() {
		Locale locale = Locale.CANADA;
		String number = "";
		
		tmp[0] = 0.0;
		tmp[UtilityPanel.RIBA] = 1.0;

		TestDialog dialog;
		for (int i = 1; i < UtilityPanel.RIBA; i++) {
			dialog = new TestDialog(tmp[i - 1], getProfit());
			dialog.setVisible(true);

			if (dialog.getReturnValue() == TestDialog.CANCEL) {
				tmp[0] = -1;
				break;
			}
			if (dialog.getValue() >= 1.0) {
				for (int j = i; j < UtilityPanel.RIBA + 1; j++) {
					tmp[j] = 1;
				}
				break;
			}
			number = NumberFormat.getNumberInstance(locale).format(dialog.getValue()); 
				//formatter.format(dialog.getValue());
			
			tmp[i] = Double.parseDouble(number);
			setProfit(dialog.getMoney());
		}

		int earning = getProfit() - Resource;
		JOptionPane.showMessageDialog(wApplet, "You started with " + Resource
				+ "\n Finished with " + getProfit() + "\n Total earnings: "
				+ earning, "Dialog", JOptionPane.INFORMATION_MESSAGE);

		return tmp;
	}

	public double[] Run2() {
		Locale locale = Locale.CANADA;
		String number = "";
		
		tmp[0] = 0.0;
		tmp[UtilityPanel.RIBA] = 1.0;

		int answer;
		double value = 0.05; // init value

		for (int i = 1; i < UtilityPanel.RIBA; i++) {
			answer = JOptionPane.NO_OPTION;
			while (answer == JOptionPane.NO_OPTION && value < 1.0) {
				String question = "You have %d. You will get %d with the probability %.2f. Do you invest?";
				question = String.format(question, new Object[] {
						Integer.valueOf(i * Resource),
						Integer.valueOf(10 * Resource),
						Double.valueOf(value)
						});
				answer = JOptionPane.showConfirmDialog(wApplet, question);
				if (answer == JOptionPane.YES_OPTION) {
					number = NumberFormat.getNumberInstance(locale).format(value); 
					
					tmp[i] = Double.parseDouble(number);
					break;
				} else if (answer == JOptionPane.CANCEL_OPTION) {
					break;
				} else if (answer == JOptionPane.NO_OPTION) {
					if (value == 0.05){
						value = 0.1;
					}
					else {
					value += 0.1;
					}
				}

			} // end while
			if (answer == JOptionPane.CANCEL_OPTION) {
				break;
			}
			//jei pasiekiamas vienetas ir zingsnis mazesnis uz max reiksme
			if (value >= 1.0 && i != UtilityPanel.RIBA) {
				for (int j = i; j < UtilityPanel.RIBA; j++) {
					tmp[j] = 1.0;
				}
			}
		}

		return tmp;
	}

	// tmp[0] = 0.0;
	// tmp[UtilityPanel.RIBA] = 1.0;
	// Object options[] = { "Yes", "No" };
	// long test = TestTime();
	// if (test == -1) {
	// tmp[0] = -1;
	// return tmp;
	// }
	// long test1 = TestTime();
	// if (test1 == -1) {
	// tmp[0] = -1;
	// return tmp;
	// }
	// test = (test + test1 * 2) / 2; // set average answer time
	// if (Begin()) {
	// tmp[0] = -1;
	// return tmp;
	// }
	// int Step = Resource / (UtilityPanel.RIBA-1);
	// int next = 0;
	// for (int i = 1; i < UtilityPanel.RIBA; i++)
	// for (int j = next; j <= UtilityPanel.RIBA+2; j++) {
	// Date oldDate = new Date();
	// long old = oldDate.getTime();
	// long invest = Step * (int)(UtilityPanel.RIBA - i);
	// long hope = Step * 25 * (UtilityPanel.RIBA - i) / 100 + Step *
	// (UtilityPanel.RIBA - i);
	// double probability = ((double) j) / (UtilityPanel.RIBA+2);
	//
	// int sel = JOptionPane.showOptionDialog(wApplet, "You invest "
	// + invest + ". After one year you will have " + hope
	// + ".\n" + "Object bankruptcy probability "
	// + probability + ".\n "
	// + "Would you like invest capital?", "Test",
	// JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
	// null, options, options[0]);
	// Date nowDate = new Date();
	// long now = nowDate.getTime();
	//
	// tmp[UtilityPanel.RIBA - i] = ((double) UtilityPanel.RIBA+2 - j) /
	// (UtilityPanel.RIBA+2);
	//				
	// if ((test < (now - old)) || (sel == 1)) { // jei daba galvojo ilgiau nei
	// per test arba atsake NO
	// next = j;
	// j = UtilityPanel.RIBA+3;
	// } else {
	// if (j == UtilityPanel.RIBA+2) {
	// next = UtilityPanel.RIBA+3;
	// for (int k = i + 1; k <= UtilityPanel.RIBA-1; k++)
	// tmp[UtilityPanel.RIBA - k] = tmp[UtilityPanel.RIBA - i];
	// }
	// }
	//
	// if (sel == -1) {
	// tmp[0] = -1;
	// return tmp;
	// }
	// }
	// return tmp;
	// }

	/**
	 * @return Returns the profit.
	 */
	private int getProfit() {
		return profit;
	}

	/**
	 * @param profit
	 *            The profit to set.
	 */
	private void setProfit(int profit) {
		this.profit = profit;
	}
}